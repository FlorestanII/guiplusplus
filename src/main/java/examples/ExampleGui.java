package examples;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.nanovg.NanoVGGL3.*;
import static org.lwjgl.opengl.GL11C.*;
import static org.lwjgl.system.MemoryUtil.*;

import java.nio.DoubleBuffer;
import java.nio.IntBuffer;
import java.util.Objects;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.opengl.GL;

import de.johannespollitt.guiplusplus.Button;
import de.johannespollitt.guiplusplus.CheckButton;
import de.johannespollitt.guiplusplus.ColorScheme;
import de.johannespollitt.guiplusplus.Frame;
import de.johannespollitt.guiplusplus.Graph;
import de.johannespollitt.guiplusplus.Gui;
import de.johannespollitt.guiplusplus.Label;
import de.johannespollitt.guiplusplus.NumberField;
import de.johannespollitt.guiplusplus.PasswordField;
import de.johannespollitt.guiplusplus.Slider;
import de.johannespollitt.guiplusplus.TextField;
import de.johannespollitt.guiplusplus.Slider.ValueListener;
import de.johannespollitt.guiplusplus.View;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

public class ExampleGui {

	public static void main(String[] args) {
		
		//Test Gui
		Gui gui = new Gui();
		Frame frame = new Frame("Example Frame");
		frame.setBounds(100, 100, 200, 300);
		gui.add(frame);
		Frame subframe1 = new Frame("Sub Frame");
		subframe1.setBounds(50, 50, 200, 100);
		frame.add(subframe1);
		
		Frame frame2 = new Frame("Example Frame 2");
		frame2.setBounds(200, 200, 500, 600);
		
		Label label = new Label("Test Label");
		label.setBounds(100, 80, 200, 20);
		frame2.add(label);
		
		Slider slider = new Slider();
		slider.setBounds(100, 100, 100, 20);
		slider.setMinValue(0);
		slider.setMaxValue(100);
		slider.setCurrentValue(50);
		slider.setEvenNumber(false);
		slider.setValueListener(new ValueListener() {
			
			@Override
			public void onValueChange(Slider slider) {
				label.setLabel("Value: " + slider.getCurrentValue());
			}
		});
		frame2.add(slider);
		
		Graph graph = new Graph();
		graph.setBounds(100, 300, 300, 100);
		graph.setMinValue(0);
		graph.setMaxValue(100);
		frame2.add(graph);
		
		Button button = new Button("Button");
		button.setBounds(100, 150, 100, 25);
		button.setAction(new Runnable() {
			
			@Override
			public void run() {
				System.out.println("Click");
			}
		});
		frame2.add(button);
		
		TextField textField = new TextField();
		textField.setBounds(100, 450, 100, 20);
		frame2.add(textField);

		PasswordField passField = new PasswordField();
		passField.setBounds(100, 480, 100, 20);
		frame2.add(passField);
		
		NumberField numberField = new NumberField();
		numberField.setBounds(300, 450, 100, 20);
		frame2.add(numberField);
		
		CheckButton check = new CheckButton("Test");
		check.setBounds(300, 480, 100, 20);
		frame2.add(check);
		
		gui.add(frame2);
		
		GLFWErrorCallback.createPrint().set();
        if (!glfwInit()) {
            throw new RuntimeException("Failed to init GLFW.");
        }
        
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        
        long window = glfwCreateWindow(1280, 720, "Gui++ Example", NULL, NULL);
        if (window == NULL) {
            glfwTerminate();
            throw new RuntimeException();
        }
        
        glfwSetKeyCallback(window, (windowHandle, keyCode, scancode, action, mods) -> {
            if (keyCode == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
                glfwSetWindowShouldClose(windowHandle, true);
            } else {
            	if (action == GLFW_PRESS) {
            		gui.pressKey(keyCode, false);
            	} else if (action == GLFW_REPEAT) {
            		gui.pressKey(keyCode, true);
            	} else if (action == GLFW_RELEASE) {
            		gui.releaseKey(keyCode);
            	}
            }
        });
        
        glfwSetCharCallback(window, (windowHandle, codepoint) -> {
        	gui.charInput((char)codepoint);
        });
        
        glfwMakeContextCurrent(window);
        GL.createCapabilities();
        glfwSwapInterval(0);
        

        long vg = nvgCreate(0);
        if (vg == NULL) {
            throw new RuntimeException("Could not init nanovg.");
        }
        
        glfwSetTime(0);
        
        IntBuffer winWidth = BufferUtils.createIntBuffer(1), winHeight = BufferUtils.createIntBuffer(1);

        IntBuffer fbWidth = BufferUtils.createIntBuffer(1), fbHeight = BufferUtils.createIntBuffer(1);
        
        DoubleBuffer mouseX = BufferUtils.createDoubleBuffer(1), mouseY = BufferUtils.createDoubleBuffer(1);
        
        View view = new View(window, vg);
        view.setColorScheme(ColorScheme.DARK_COLOR_SCHEME);
        
        view.setPlainFont("assets/guiplusplus/fonts/OpenSans-Regular.ttf");
        view.setIconFont("assets/guiplusplus/fonts/open-iconic.ttf");

        glfwSetMouseButtonCallback(window, (windowHandle, mouse, action, mods) -> {
        	gui.clickMouse(mouse, action, view);
        });
        
        float[] lastTimes = new float[100];
        
        long lastFrame = System.nanoTime();
        long timepassed = 0;
        int ticks = 0;
        int tps = 0;
        
        while (!glfwWindowShouldClose(window)) {
        	
        	long thisFrame = System.nanoTime();
        	long tslf = thisFrame-lastFrame;
        	timepassed += tslf;
        	lastFrame = thisFrame;
        	ticks++;
        	
        	if (timepassed >= 1000000000) {
    			tps = ticks;
    			ticks = 0;
    			timepassed = timepassed % 1000000000;
    			
    			for (int i = 1; i < lastTimes.length; i++) {
    				lastTimes[i-1] = lastTimes[i];
    			}
    			
    			lastTimes[lastTimes.length-1] = tps;
    			graph.setValues(lastTimes);
    		}
        	
        	glClearColor(0.3f, 0.3f, 0.5f, 0.3f);
        	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        	
        	glfwGetWindowSize(window, winWidth, winHeight);
            glfwGetFramebufferSize(window, fbWidth, fbHeight);
            glfwGetCursorPos(window, mouseX, mouseY);
            glViewport(0, 0, winWidth.get(0), winHeight.get(0));
        	
            view.updateFramebufferSize(fbWidth.get(0), fbHeight.get(0));
            view.setViewport(0, 0, fbWidth.get(0), fbHeight.get(0));
            View.setMouseX((int) mouseX.get(0));
            View.setMouseY((int) mouseY.get(0));
            
        	gui.render(view);
        	
        	glfwSwapBuffers(window);
        	glfwPollEvents();

        	try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        }
        
        nvgDelete(vg);
        GL.setCapabilities(null);
        
        glfwFreeCallbacks(window);
        glfwTerminate();
        Objects.requireNonNull(glfwSetErrorCallback(null)).free();
        
	}
	
}
