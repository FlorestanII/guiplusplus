package de.johannespollitt.guiplusplus;

import java.util.ArrayList;

import static org.lwjgl.nanovg.NanoVG.*;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

public abstract class GuiContainer extends GuiComponent {
	
	private final ArrayList<GuiComponent> components;
	
	public GuiContainer() {
		this.components = new  ArrayList<GuiComponent>();
	}
	
	public void add(GuiComponent component) {
		component.setParent(this);
		this.components.add(component);
	}
	
	public void renderChildren(View view) {
		for (GuiComponent component: this.components) {
			if (component.isVisible()) {
				int left = view.getLeft();
				int right = view.getRight();
				int top = view.getTop();
				int bottom = view.getBottom();
				view.subViewport(view.getLeft() + component.getX(), view.getTop() + component.getY(), view.getLeft() + component.getX() + component.getWidth(), view.getTop() + component.getY() + component.getHeight());
				nvgSave(view.getVG());
				nvgTranslate(view.getVG(), component.getX(), component.getY());
				
				component.render(view);
				
				if (component instanceof GuiContainer) {
					nvgScissor(view.getVG(), 0, 0, component.getWidth(), component.getHeight());
					((GuiContainer)component).renderChildren(view);
					//nvgResetScissor(view.getVG());
				}
				
				//nvgTranslate(view.getVG(), -component.getX(), -component.getY());
				nvgRestore(view.getVG());
				view.setViewport(left, top, right, bottom);
			}
		}
	}
	
	public boolean moveMouseChildren(int mouseX, int mouseY, int lastMouseX, int lastMouseY, View view) {
		for (int i = this.components.size()-1; i >= 0; i--) {
			GuiComponent component = this.components.get(i);
			if (component.isVisible()) {
				int left = view.getLeft();
				int top = view.getTop();
				int right = view.getRight();
				int bottom = view.getBottom();
				view.subViewport(view.getLeft() + component.getX(), view.getTop() + component.getY(), view.getLeft() + component.getX() + component.getWidth(), view.getTop() + component.getY() + component.getHeight());
				if (view.isMouseInView()) {
					if (component instanceof GuiContainer) {
						if (((GuiContainer)component).moveMouseChildren(mouseX, mouseY, lastMouseX, lastMouseY, view)) {
							return true;
						}
					}
					if (component.moveMouse(mouseX, mouseY, lastMouseX, lastMouseY, view)) {
						return true;
					}
					return true;
				} else {
					if (component instanceof GuiContainer) {
						((GuiContainer)component).moveMouseOutsideChildren();
					}
					component.moveMouseOutside();
				}
				view.setViewport(left, top, right, bottom);
			}
		}
		return false;
	}
	
	public void moveMouseOutsideChildren() {
		for (int i = this.components.size()-1; i >= 0; i--) {
			GuiComponent component = this.components.get(i);
			
			if (component.isVisible()) {
				if (component instanceof GuiContainer) {
					((GuiContainer) component).moveMouseOutsideChildren();
				}
				component.moveMouseOutside();
			}
			
		}
	}
	
	public boolean clickMouseChildren(int button, int action, View view) {
		for (int i = this.components.size()-1; i >= 0; i--) {
			GuiComponent component = this.components.get(i);
			if (component.isVisible()) {
				int left = view.getLeft();
				int top = view.getTop();
				int right = view.getRight();
				int bottom = view.getBottom();
				view.subViewport(view.getLeft() + component.getX(), view.getTop() + component.getY(), view.getLeft() + component.getX() + component.getWidth(), view.getTop() + component.getY() + component.getHeight());
				if (view.isMouseInView()) {
					if (component instanceof GuiContainer) {
						if (((GuiContainer)component).clickMouseChildren(button, action, view)) {
							return true;
						}
					}
					if (component.clickMouse(button, action, view)) {
						return true;
					}
					return true;
				} else {
					if (component instanceof GuiContainer) {
						((GuiContainer)component).clickOutsideChildren();
					}
					component.clickOutside();
				}
				view.setViewport(left, top, right, bottom);
			}
		}
		return false;
	}
	
	public void clickOutsideChildren() {
		for (int i = this.components.size()-1; i >= 0; i--) {
			GuiComponent component = this.components.get(i);
			
			if (component.isVisible()) {
				if (component instanceof GuiContainer) {
					((GuiContainer) component).clickOutsideChildren();
				}
				component.clickOutside();
			}
			
		}
	}
	
	public boolean pressKeyChildren(int key, boolean repeated) {
		for (int i = this.components.size()-1; i >= 0; i--) {
			GuiComponent component = this.components.get(i);
			if (component.isVisible()) {
				if (component instanceof GuiContainer) {
					if (((GuiContainer)component).pressKeyChildren(key, repeated)) {
						return true;
					}
				}
				if (component.pressKey(key, repeated)) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean releaseKeyChildren(int key) {
		for (int i = this.components.size()-1; i >= 0; i--) {
			GuiComponent component = this.components.get(i);
			if (component.isVisible()) {
				if (component instanceof GuiContainer) {
					if (((GuiContainer)component).releaseKeyChildren(key)) {
						return true;
					}
				}
				if (component.releaseKey(key)) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean charInputChildren(char c) {
		for (int i = this.components.size()-1; i >= 0; i--) {
			GuiComponent component = this.components.get(i);
			if (component.isVisible()) {
				if (component instanceof GuiContainer) {
					if (((GuiContainer)component).charInputChildren(c)) {
						return true;
					}
				}
				if (component.charInput(c)) {
					return true;
				}
			}
		}
		return false;
	}
	
}
