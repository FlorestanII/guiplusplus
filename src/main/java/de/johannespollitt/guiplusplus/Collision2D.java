package de.johannespollitt.guiplusplus;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

public class Collision2D {

	private Collision2D() {
		
	}
	
	public static boolean pointBox(int px, int py, int bx, int by, int width, int height) {
		if (px >= bx && px <= bx + width) {
			if (py >= by && py <= by + height) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean pointBox2P(int px, int py, int bx1, int by1, int bx2, int by2) {
		if (px >= bx1 && px <= bx2) {
			if (py >= by1 && py <= by2) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean pointCircle(int px, int py, int cx, int cy, int cr) {
		if (((px-cx)*(px-cx))+((py-cy)*(py-cy)) <= cr*cr) {
			return true;
		}
		return false;
	}
	
}
