package de.johannespollitt.guiplusplus;

import static org.lwjgl.nanovg.NanoVG.*;

import org.lwjgl.nanovg.NVGPaint;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

public class Slider extends GuiComponent {

	private float minValue;
	private float maxValue;
	private float currentValue;

	private boolean evenNumbers = false;
	
	private boolean dragging = false;
	
	private ValueListener valueListener;
	
	public Slider() {
		
	}

	@Override
	public void render(View view) {
		
		//Background
		nvgBeginPath(view.getVG());
		nvgRoundedRect(view.getVG(), view.getCornerRadius()*3, getHeight()/2-view.getCornerRadius(), getWidth()-view.getCornerRadius()*6, view.getCornerRadius()*2, view.getCornerRadius());
		nvgFillColor(view.getVG(), view.rgba(0, 0, 0, 0.1f, view.colorA));
		nvgFill(view.getVG());
		
		//Left side
		nvgBeginPath(view.getVG());
		nvgRoundedRect(view.getVG(), view.getCornerRadius()*3, getHeight()/2-view.getCornerRadius(), (currentValue/(maxValue-minValue))*(getWidth()-view.getCornerRadius()*6), view.getCornerRadius()*2, view.getCornerRadius());
		nvgFillColor(view.getVG(), view.rgba(0.3f, 0.2f, 0.2f, 0.5f, view.colorA));
		nvgFill(view.getVG());
		
		//Drop Shadow
		NVGPaint shadowPaint = view.paintA;
		nvgBoxGradient(view.getVG(), view.getCornerRadius()*3, getHeight()/2-view.getCornerRadius(), getWidth()-view.getCornerRadius()*6, view.getCornerRadius()*2, view.getCornerRadius() * 2, 10, view.getColorScheme().getShadowColor(), view.rgba(0.0f, 0.0f, 0.0f, 0.0f, view.colorB), shadowPaint);
		nvgBeginPath(view.getVG());
		nvgRoundedRect(view.getVG(), view.getCornerRadius()*3-5, getHeight()/2-5, (getWidth()-view.getCornerRadius()*6) + 10, view.getCornerRadius()*2 + 10, view.getCornerRadius());
		nvgRoundedRect(view.getVG(), view.getCornerRadius()*3, getHeight()/2-view.getCornerRadius(), (getWidth()-view.getCornerRadius()*6), view.getCornerRadius()*2, view.getCornerRadius());
		nvgPathWinding(view.getVG(), NVG_HOLE);
		nvgFillPaint(view.getVG(), shadowPaint);
		nvgFill(view.getVG());
		
		//Regulator circle
		nvgBeginPath(view.getVG());
		nvgCircle(view.getVG(), (currentValue/(maxValue-minValue))*(getWidth()-view.getCornerRadius()*6)+view.getCornerRadius()*3, getHeight()/2, view.getCornerRadius()*3);
		nvgFillColor(view.getVG(), view.rgba(0.4f, 0.4f, 0.4f, 0.9f, view.colorA));
		nvgFill(view.getVG());
		
		//Drop Shadow Circle
		nvgBoxGradient(view.getVG(), (currentValue/(maxValue-minValue))*(getWidth()-view.getCornerRadius()*6), getHeight()/2-view.getCornerRadius()*3, view.getCornerRadius()*6, view.getCornerRadius()*6, view.getCornerRadius()*3, 10, view.getColorScheme().getShadowColor(), view.rgba(0.0f, 0.0f, 0.0f, 0.0f, view.colorB), shadowPaint);
		nvgBeginPath(view.getVG());
		nvgCircle(view.getVG(), (currentValue/(maxValue-minValue))*(getWidth()-view.getCornerRadius()*6)+view.getCornerRadius()*3, getHeight()/2, view.getCornerRadius()*9);
		nvgCircle(view.getVG(), (currentValue/(maxValue-minValue))*(getWidth()-view.getCornerRadius()*6)+view.getCornerRadius()*3, getHeight()/2, view.getCornerRadius()*3);
		nvgPathWinding(view.getVG(), NVG_HOLE);
		nvgFillPaint(view.getVG(), shadowPaint);
		nvgFill(view.getVG());
	}

	@Override
	public boolean moveMouse(int mouseX, int mouseY, int lastMouseX, int lastMouseY, View view) {
		if (dragging) {
			setCurrentValue(((View.getMouseX()-(getAbsoluteX()+view.getCornerRadius()*3))*(maxValue-minValue))/(getWidth()-view.getCornerRadius()*6));
		}

		return false;
	}

	@Override
	public boolean clickMouse(int button, int action, View view) {
		if (button == 0 && action == 1) {

			if (!Collision2D.pointCircle(View.getMouseX(), View.getMouseY(), (int)(getAbsoluteX()+view.getCornerRadius()*3+(currentValue/(maxValue-minValue))*(getWidth()-view.getCornerRadius()*6)), getAbsoluteY()+getHeight()/2, (int)(view.getCornerRadius()*3)) && view.isMouseInView()) {
				setCurrentValue(((View.getMouseX()-(getAbsoluteX()+view.getCornerRadius()*3))*(maxValue-minValue))/(getWidth()-view.getCornerRadius()*6));
			}
			dragging = true;
			
		} else if (action == 0 && button == 0){
			setCurrentValue(((View.getMouseX()-(getAbsoluteX()+view.getCornerRadius()*3))*(maxValue-minValue))/(getWidth()-view.getCornerRadius()*6));
			dragging = false;
		}
		
		return false;
	}
	
	@Override
	public void moveMouseOutside() {
		dragging = false;
	}
	
	@Override
	public boolean pressKey(int key, boolean repeated) {
		return false;
	}

	@Override
	public boolean releaseKey(int key) {
		return false;
	}
	
	public float getMinValue() {
		return this.minValue;
	}
	
	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}
	
	public float getMaxValue() {
		return this.maxValue;
	}
	
	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}
	
	public float getCurrentValue() {
		return this.currentValue;
	}
	
	public void setCurrentValue(float newValue) {
		
		if (evenNumbers) {
			newValue = Math.round(newValue);
		}
		
		if (newValue < minValue) {
			newValue = minValue;
		}
		if (newValue > maxValue) {
			newValue = maxValue;
		}
		this.currentValue = newValue;
		if (this.valueListener != null) {
			valueListener.onValueChange(this);
		}
	}
	
	public void setEvenNumber(boolean evennumbers) {
		this.evenNumbers = evennumbers;
	}
	
	public boolean isEvenNumbersEnabled() {
		return this.evenNumbers;
	}
	
	public void setValueListener(ValueListener listener) {
		this.valueListener = listener;
	}
	
	public interface ValueListener {
		
		public void onValueChange(Slider slider);
		
	}
	
}
