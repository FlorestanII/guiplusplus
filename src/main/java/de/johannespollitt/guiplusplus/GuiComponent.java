package de.johannespollitt.guiplusplus;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

public abstract class GuiComponent {

	private boolean visible = true;
	
	private int x;
	private int y;
	
	private int width;
	private int height;
	
	private GuiContainer parent;
	
	public GuiComponent() {
		
	}
	
	public abstract void render(View view);
	
	public abstract boolean moveMouse(int mouseX, int mouseY, int lastMouseX, int lastMouseY, View view);
	public void moveMouseOutside() {}
	
	public abstract boolean clickMouse(int button, int action, View view);

	public void clickOutside() {}
	
	public abstract boolean pressKey(int key, boolean repeated);
	
	public abstract boolean releaseKey(int key);
	
	public boolean charInput(char c) {
		return false;
	}
	
	public  void setVisible(boolean visible) {
		this.visible = visible;
	}
	
	public boolean isVisible() {
		return this.visible;
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y ) {
		this.y = y;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public void setBounds(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	public void setParent(GuiContainer container) {
		this.parent = container;
	}
	
	public GuiContainer getParent() {
		return this.parent;
	}
	
	public int getAbsoluteX() {
		if (this.parent == null) {
			return this.x;
		} else {
			return this.parent.getAbsoluteX() + this.x;
		}
	}
	
	public int getAbsoluteY() {
		if (this.parent == null) {
			return this.y;
		} else {
			return this.parent.getAbsoluteY() + this.y;
		}
	}

	public boolean isPointInside(int px, int py) {
		if (parent == null) {
			return Collision2D.pointBox(px, py, getAbsoluteX(), getAbsoluteY(), getWidth(), getHeight());
		} else {
			return Collision2D.pointBox(px, py, getAbsoluteX(), getAbsoluteY(), getWidth(), getHeight()) && parent.isPointInside(px, py);
		}
	}
	
}
