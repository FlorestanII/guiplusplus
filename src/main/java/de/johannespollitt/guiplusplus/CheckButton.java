package de.johannespollitt.guiplusplus;

import static org.lwjgl.nanovg.NanoVG.*;
import static org.lwjgl.system.MemoryUtil.*;

import java.nio.ByteBuffer;

import org.lwjgl.nanovg.NVGPaint;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

public class CheckButton extends GuiComponent {

	private String text;
	private ByteBuffer textBuffer;
	
	private boolean checked = false;
	
	private Runnable action;
	
	public CheckButton(String text) {
		this.text = text;
		this.textBuffer = memUTF8(this.text, false);
	}

	@Override
	public void render(View view) {
		//Background
		nvgBeginPath(view.getVG());
		nvgRoundedRect(view.getVG(), view.getCornerRadius(), view.getCornerRadius(), getHeight()-view.getCornerRadius()*2, getHeight()-view.getCornerRadius()*2, view.getCornerRadius());
		nvgFillColor(view.getVG(), view.rgba(0.21f, 0.23f, 0.24f, 1.0f, view.colorA));
		nvgFill(view.getVG());
		
		nvgBeginPath(view.getVG());
		nvgRoundedRect(view.getVG(), 0, 0, getHeight(), getHeight(), view.getCornerRadius()*2);
		nvgRoundedRect(view.getVG(), view.getCornerRadius(), view.getCornerRadius(), getHeight()-view.getCornerRadius()*2, getHeight()-view.getCornerRadius()*2, view.getCornerRadius());
		nvgPathWinding(view.getVG(), NVG_HOLE);
		nvgFillColor(view.getVG(), view.rgba(0.29f, 0.30f, 0.31f, 1.0f, view.colorA));
		nvgFill(view.getVG());
		
		//Drop Shadow
		NVGPaint shadowPaint = view.paintA;
		nvgBoxGradient(view.getVG(), 0, 0 + 2, view.getCornerRadius()*6, view.getCornerRadius()*6, view.getCornerRadius() * 2, 10, view.getColorScheme().getShadowColor(), view.rgba(0.0f, 0.0f, 0.0f, 0.0f, view.colorB), shadowPaint);
		nvgBeginPath(view.getVG());
		nvgRoundedRect(view.getVG(), -10, -10, view.getCornerRadius()*6 + 20, view.getCornerRadius()*6 + 20, view.getCornerRadius()*2);
		nvgRoundedRect(view.getVG(), 0, 0, view.getCornerRadius()*6, view.getCornerRadius()*6, view.getCornerRadius());
		nvgPathWinding(view.getVG(), NVG_HOLE);
		nvgFillPaint(view.getVG(), shadowPaint);
		nvgFill(view.getVG());
		
		if (this.checked) {
			view.renderString(view.icon_hook, getHeight()/2, getHeight()/2, getHeight()*0.8f, "icons", view.rgba(0, 0.5f, 0, 1, view.colorA), NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
		}
		
		view.renderString(textBuffer, getHeight() + view.getCornerRadius(), getHeight()/2, getHeight(), "plain", view.rgba(1, 1, 1, 1, view.colorA), NVG_ALIGN_LEFT | NVG_ALIGN_MIDDLE);
	}

	@Override
	public boolean moveMouse(int mouseX, int mouseY, int lastMouseX, int lastMouseY, View view) {
		return false;
	}
	
	@Override
	public boolean clickMouse(int button, int action, View view) {
		if (action == 1 && button == 0) {
			this.checked = !this.checked;
			if (this.action != null) {
				this.action.run();
			}
		}
		return true;
	}

	@Override
	public boolean pressKey(int key, boolean repeated) {
		return false;
	}

	@Override
	public boolean releaseKey(int key) {
		return false;
	}
	
	public void setText(String text) {
		this.text = text;
		memFree(textBuffer);
		this.textBuffer = memUTF8(text, false);
	}
	
	public String getText() {
		return this.text;
	}
	
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
	public boolean isChecked() {
		return this.checked;
	}

	public void setAction(Runnable action) {
		this.action = action;
	}
	
}
