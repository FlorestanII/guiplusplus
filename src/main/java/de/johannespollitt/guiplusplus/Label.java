package de.johannespollitt.guiplusplus;

import static org.lwjgl.nanovg.NanoVG.*;
import static org.lwjgl.system.MemoryUtil.*;

import java.nio.ByteBuffer;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

public class Label extends GuiComponent {

	private String label;
	private ByteBuffer labelBuffer;

	public Label(String label) {
		this.label = label;
		this.labelBuffer = memUTF8(label, false);
	}

	@Override
	public void render(View view) {
		view.renderString(labelBuffer, 0, getHeight()/2, getHeight(), "plain", view.getColorScheme().getFrameTitleColor(), NVG_ALIGN_LEFT | NVG_ALIGN_MIDDLE);
	}

	@Override
	public boolean moveMouse(int mouseX, int mouseY, int lastMouseX, int lastMouseY, View view) {
		return false;
	}

	@Override
	public boolean clickMouse(int button, int action, View view) {
		return false;
	}

	@Override
	public boolean pressKey(int key, boolean repeated) {
		return false;
	}

	@Override
	public boolean releaseKey(int key) {
		return false;
	}
	
	public String getLabel() {
		return this.label;
	}
	
	public void setLabel(String label) {
		this.label = label;
		memFree(labelBuffer);
		this.labelBuffer = memUTF8(label, false);
	}

}
