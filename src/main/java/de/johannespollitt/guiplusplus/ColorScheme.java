package de.johannespollitt.guiplusplus;

import org.lwjgl.nanovg.NVGColor;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

public class ColorScheme {
	
	public static final ColorScheme DEFAULT_COLOR_SCHEME = new ColorScheme();
	public static final ColorScheme DARK_COLOR_SCHEME = new ColorScheme();
	public static final ColorScheme LIGHT_COLOR_SCHEME = new ColorScheme();
	
	static {
		DARK_COLOR_SCHEME.setShadowColor(0, 0, 0, 0.5f).setFrameBackgroundColor(0.17f, 0.18f, 0.19f, 1.0f).setFrameTitleColor(0.86f, 0.86f, 0.86f, 0.63f);
		DARK_COLOR_SCHEME.setFrameCloseButtonColor(1.0f, 0.3f, 0.3f, 0.8f).setFrameCloseButtonHoverColor(0.8f, 0.2f, 0.2f, 0.8f);
	}
	
	private NVGColor frameBackgroundColor;
	private NVGColor shadowColor;
	private NVGColor frameTitleColor;
	private NVGColor frameCloseButtonColor;
	private NVGColor frameCloseButtonHoverColor;
	
	public ColorScheme() {
		this.frameBackgroundColor = NVGColor.create();
		this.shadowColor = NVGColor.create();
		this.frameTitleColor = NVGColor.create();
		this.frameCloseButtonColor = NVGColor.create();
		this.frameCloseButtonHoverColor = NVGColor.create();
	}
	
	private NVGColor rgba(float r, float g, float b, float a, NVGColor color) {
		color.r(r);
		color.g(g);
		color.b(b);
		color.a(a);
		
		return color;
	}
	
	public ColorScheme setFrameBackgroundColor(float r, float g, float b, float a) {
		rgba(r, g, b, a, this.frameBackgroundColor);
		return this;
	}
	
	public NVGColor getFrameBackgroundColor() {
		return this.frameBackgroundColor;
	}
	
	public ColorScheme setShadowColor(float r, float g, float b, float a) {
		rgba(r, g, b, a, shadowColor);
		return this;
	}
	
	public NVGColor getShadowColor() {
		return this.shadowColor;
	}
	
	public ColorScheme setFrameTitleColor(float r, float g, float b, float a) {
		rgba(r, g, b, a, frameTitleColor);
		return this;
	}
	
	public NVGColor getFrameTitleColor() {
		return this.frameTitleColor;
	}

	public ColorScheme setFrameCloseButtonColor(float r, float g, float b, float a) {
		rgba(r, g, b, a, frameCloseButtonColor);
		return this;
	}
	
	public NVGColor getFrameCloseButtonColor() {
		return this.frameCloseButtonColor;
	}
	
	public ColorScheme setFrameCloseButtonHoverColor(float r, float g, float b, float a) {
		rgba(r, g, b, a, frameCloseButtonHoverColor);
		return this;
	}
	
	public NVGColor getFrameCloseButtonHoverColor() {
		return this.frameCloseButtonHoverColor;
	}
	
}
