package de.johannespollitt.guiplusplus;

import static org.lwjgl.nanovg.NanoVG.*;
import static org.lwjgl.system.MemoryUtil.*;

import java.nio.ByteBuffer;

import org.lwjgl.nanovg.NVGPaint;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

public class Frame extends GuiContainer {
	
	private String title;
	private ByteBuffer titleBuffer;
	
	private boolean closable = true;
	
	private boolean mouseOverCloseButton = false;
	private boolean mouseOverHeader = false;
	
	private boolean draggable = true;
	
	private boolean dragging = false;
	
	public Frame() {
		this("");
	}
	
	public Frame(String title) {
		this.title = title;
		this.titleBuffer = memUTF8(title, false);
	}
	
	@Override
	public void render(View view) {
		
		//Background
		nvgBeginPath(view.getVG());
		nvgRoundedRect(view.getVG(), 0, 0, getWidth(), getHeight(), view.getCornerRadius());
		nvgFillColor(view.getVG(), view.getColorScheme().getFrameBackgroundColor());
		nvgFill(view.getVG());
		
		//Drop Shadow
		NVGPaint shadowPaint = view.paintA;
		nvgBoxGradient(view.getVG(), 0, 0 + 2, getWidth(), getHeight(), view.getCornerRadius() * 2, 10, view.getColorScheme().getShadowColor(), view.rgba(0.0f, 0.0f, 0.0f, 0.0f, view.colorB), shadowPaint);
		nvgBeginPath(view.getVG());
		nvgRoundedRect(view.getVG(), -10, -10, getWidth() + 20, getHeight() + 30, view.getCornerRadius());
		nvgRoundedRect(view.getVG(), 0, 0, getWidth(), getHeight(), view.getCornerRadius());
		nvgPathWinding(view.getVG(), NVG_HOLE);
		nvgFillPaint(view.getVG(), shadowPaint);
		nvgFill(view.getVG());
		
		//Header
		NVGPaint headerPaint = view.paintB;
		if (mouseOverHeader) {
			nvgLinearGradient(view.getVG(), 0, 0, 0, 0 + view.getHeaderHeight()/2, view.rgba(1.0f, 1.0f, 1.0f, 0.25f, view.colorA), view.rgba(0.0f, 0.0f, 0.0f, 0.25f, view.colorB), headerPaint);
		} else {
			nvgLinearGradient(view.getVG(), 0, 0, 0, 0 + view.getHeaderHeight()/2, view.rgba(1.0f, 1.0f, 1.0f, 0.15f, view.colorA), view.rgba(0.0f, 0.0f, 0.0f, 0.15f, view.colorB), headerPaint);
		}
		nvgBeginPath(view.getVG());
		nvgRoundedRect(view.getVG(), 1, 1, getWidth() - 2, view.getHeaderHeight(), view.getCornerRadius() - 1);
		nvgFillPaint(view.getVG(), headerPaint);
		nvgFill(view.getVG());
		
		nvgBeginPath(view.getVG());
		nvgMoveTo(view.getVG(), 0.5f, 0.5f + view.getHeaderHeight());
        nvgLineTo(view.getVG(), 0.5f + getWidth() - 1, 0.5f + view.getHeaderHeight());
        nvgStrokeColor(view.getVG(), view.rgba(0.0f, 0.0f, 0.0f, 0.12f, view.colorA));
        nvgStroke(view.getVG());
		
		//Title
		view.renderString(titleBuffer, getWidth()/2, view.getHeaderHeight()/2, view.getHeaderHeight()*0.875f, "plain", view.getColorScheme().getFrameTitleColor(), NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
		
		//Closebutton
		if (closable) {
			if (mouseOverCloseButton) {
				view.renderString(view.icon_close, getWidth()-view.getHeaderHeight()/2, view.getHeaderHeight()/2+2, view.getHeaderHeight()*0.75f, "icons", view.getColorScheme().getFrameCloseButtonHoverColor(), NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
			} else {
				view.renderString(view.icon_close, getWidth()-view.getHeaderHeight()/2, view.getHeaderHeight()/2+2, view.getHeaderHeight()*0.75f, "icons", view.getColorScheme().getFrameCloseButtonColor(), NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
			}
		}
		
	}
	
	@Override
	public boolean moveMouse(int mouseX, int mouseY, int lastMouseX, int lastMouseY, View view) {
		
		if (Collision2D.pointBox2P(View.getMouseX(), View.getMouseY(), getAbsoluteX(), getAbsoluteY(), getAbsoluteX() + getWidth(), getAbsoluteY() + view.getHeaderHeight()) && view.isMouseInView()) {
			mouseOverHeader = true;
			
			if (closable) {
				if (Collision2D.pointCircle(View.getMouseX(), View.getMouseY(), getAbsoluteX() + getWidth()-view.getHeaderHeight()/2, getAbsoluteY() + view.getHeaderHeight()/2+2, (int)(view.getHeaderHeight()*0.75f)/2)) {
					mouseOverCloseButton = true;
				} else {
					mouseOverCloseButton = false;
				}
			} else {
				mouseOverCloseButton = false;
			}
			
			//Dragging
			if (view.isMouseButtonPressed(0) && draggable && dragging) {
				int dx = lastMouseX - mouseX;
				int dy = lastMouseY - mouseY;
				setX(getX() - dx);
				setY(getY() - dy);
			}
			
		} else {
			mouseOverHeader = false;
			dragging = false;
		}
		
		return false;
	}
	
	@Override
	public void moveMouseOutside() {
		mouseOverCloseButton = false;
		mouseOverHeader = false;
	}
	
	@Override
	public boolean clickMouse(int button, int action, View view) {
		if (button == 0 && action == 1) {
			if (mouseOverCloseButton) {
				setVisible(false);
				return true;
			} else if (mouseOverHeader && draggable) {
				dragging = true;
				return true;
			}
		} else if (button == 0 && action == 0) {
			dragging = false;
		}
		return false;
	}
	
	@Override
	public boolean pressKey(int key, boolean repeated) {
		return false;
	}
	
	@Override
	public boolean releaseKey(int key) {
		return false;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public void setTitle(String title) {
		this.title = title;
		memFree(this.titleBuffer);
		this.titleBuffer = memUTF8(title, false);
	}
	
	public boolean isClosable() {
		return this.closable;
	}
	
	public void setClosable(boolean closable) {
		this.closable = closable;
	}
	
	public boolean isDraggable() {
		return this.draggable;
	}
	
	public void setDraggable(boolean draggable) {
		this.draggable = draggable;
	}
	
}
