package de.johannespollitt.guiplusplus;

import static org.lwjgl.nanovg.NanoVG.*;
import static org.lwjgl.system.MemoryUtil.*;

import java.nio.ByteBuffer;

import org.lwjgl.nanovg.NVGPaint;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

public class Graph extends GuiContainer {

	private float[] values = new float[10];
	
	private float minValue = Float.POSITIVE_INFINITY;
	private ByteBuffer minBuffer;
	
	private float maxValue = Float.NEGATIVE_INFINITY;
	private ByteBuffer maxBuffer;
	
	public Graph() {
		
	}

	@Override
	public void render(View view) {
		//Background
		nvgBeginPath(view.getVG());
		nvgRect(view.getVG(), 0, 0, getWidth(), getHeight());
		nvgFillColor(view.getVG(), view.rgba(0.2f, 0.2f, 0.2f, 0.8f, view.colorA));
		nvgFill(view.getVG());
		
		//Drop Shadow
		NVGPaint shadowPaint = view.paintA;
		nvgBoxGradient(view.getVG(), 0, 0 + 2, getWidth(), getHeight(), view.getCornerRadius() * 2, 10, view.getColorScheme().getShadowColor(), view.rgba(0.0f, 0.0f, 0.0f, 0.0f, view.colorB), shadowPaint);
		nvgBeginPath(view.getVG());
		nvgRoundedRect(view.getVG(), -10, -10, getWidth() + 20, getHeight() + 20, view.getCornerRadius());
		nvgRect(view.getVG(), 0, 0, getWidth(), getHeight());
		nvgPathWinding(view.getVG(), NVG_HOLE);
		nvgFillPaint(view.getVG(), shadowPaint);
		nvgFill(view.getVG());
	}
	
	@Override
	public void renderChildren(View view) {
		nvgBeginPath(view.getVG());
		nvgMoveTo(view.getVG(), 0, view.getCornerRadius());
		nvgLineTo(view.getVG(), getWidth(), view.getCornerRadius());
    	
    	nvgMoveTo(view.getVG(), 0, getHeight()*0.75f);
    	nvgLineTo(view.getVG(), getWidth(), getHeight()*0.75f);
		
    	nvgMoveTo(view.getVG(), 0, getHeight()*0.5f);
    	nvgLineTo(view.getVG(), getWidth(), getHeight()*0.5f);
    	
    	nvgMoveTo(view.getVG(), 0, getHeight()*0.25f);
    	nvgLineTo(view.getVG(), getWidth(), getHeight()*0.25f);
		
    	nvgMoveTo(view.getVG(), 0, getHeight() - view.getCornerRadius());
    	nvgLineTo(view.getVG(), getWidth(), getHeight() - view.getCornerRadius());
    	nvgStrokeColor(view.getVG(), view.rgba(0.5f, 0.5f, 0.5f, 1.0f, view.colorA));
    	nvgStroke(view.getVG());
        
    	if (this.values != null && minValue < maxValue) {
			
			int height = (int) (getHeight() - view.getCornerRadius()*2);
			
			nvgBeginPath(view.getVG());
			nvgMoveTo(view.getVG(), 0.5f, view.getCornerRadius() + height - (height/(maxValue-minValue)*(values[0]-minValue)));
	        
	        for (int i = 1; i < values.length; i++) {
	        	nvgLineTo(view.getVG(), 0.5f + (getWidth()/(values.length-1))*i, view.getCornerRadius() + height - (height/(maxValue-minValue)*(values[i]-minValue)));
	        }
	        
	        nvgStrokeColor(view.getVG(), view.rgba(1.0f, 0.84f, 0.0f, 1.0f, view.colorA));
	        nvgStroke(view.getVG());
			
		}
		
        view.renderString(minBuffer, 0.5f, getHeight()-view.getCornerRadius(), getHeight()*0.15f, "plain", view.rgba(0.5f, 0.5f, 0.5f, 1.0f, view.colorA), NVG_ALIGN_LEFT | NVG_ALIGN_BOTTOM);
        view.renderString(maxBuffer, 0.5f, view.getCornerRadius(), getHeight()*0.15f, "plain", view.rgba(0.5f, 0.5f, 0.5f, 1.0f, view.colorA), NVG_ALIGN_LEFT | NVG_ALIGN_TOP);
	}
	
	@Override
	public boolean moveMouse(int mouseX, int mouseY, int lastMouseX, int lastMouseY, View view) {
		return false;
	}

	@Override
	public boolean clickMouse(int button, int action, View view) {
		return false;
	}

	@Override
	public boolean pressKey(int key, boolean repeated) {
		return false;
	}

	@Override
	public boolean releaseKey(int key) {
		return false;
	}
	
	public void setValues(float[] values) {
		this.values = values;
	}
	
	public float[] getValues() {
		return this.values;
	}
	
	public void setMinValue(float minValue) {
		this.minValue = minValue;
		memFree(this.minBuffer);
		this.minBuffer = memUTF8(Float.toString(this.minValue), false);
	}
	
	public float getMinValue() {
		return this.minValue;
	}
	
	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
		memFree(this.maxBuffer);
		this.maxBuffer = memUTF8(Float.toString(this.maxValue), false);
	}
	
	public float getMaxValue() {
		return this.maxValue;
	}
	
}
