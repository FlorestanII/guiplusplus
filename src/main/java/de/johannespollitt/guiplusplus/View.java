package de.johannespollitt.guiplusplus;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.lwjgl.nanovg.NVGColor;
import org.lwjgl.nanovg.NVGPaint;
import org.lwjgl.system.MemoryStack;

import static org.lwjgl.nanovg.NanoVG.*;
import static org.lwjgl.system.MemoryUtil.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.glfw.GLFW.*;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

public class View {
	
	private static int mouseX;
	private static int mouseY;
	
	private ColorScheme colorScheme;
	
	private final long windowhandle;
	private final long vgHandle;
	
	private int framebufferWidth;
	private int framebufferHeight;
	
	private int left;
	private int top;
	private int right;
	private int bottom;
	
	private boolean isMouseInView = false;
	
	//Fonts
	private ByteBuffer iconFont;
	private ByteBuffer plainFont;
	
	//Icons
	public final ByteBuffer icon_close = memUTF8(new String(Character.toChars(0xe039)), false);
	public final ByteBuffer icon_minimized = memUTF8(new String(Character.toChars(0xe02f)),false);
	public final ByteBuffer icon_maximized = memUTF8(new String(Character.toChars(0xe02d)), false);
	public final ByteBuffer icon_hook = memUTF8(new String(Character.toChars(0xe033)), false);
	
	public final ByteBuffer icon_cursor = memUTF8("|", false);
	
	public NVGColor colorA;
	public NVGColor colorB;
	public NVGColor colorC;
	
	public NVGPaint paintA;
	public NVGPaint paintB;
	public NVGPaint paintC;
	
	private int headerHeight = 20;
	
	private float cornerRadius = 3.0f;
	
	public View(long windowhandle, long vgHandle) {
		this.colorScheme = ColorScheme.DEFAULT_COLOR_SCHEME;
		this.vgHandle = vgHandle;
		this.windowhandle = windowhandle;
		
		this.colorA = NVGColor.create();
		this.colorB = NVGColor.create();
		this.colorC = NVGColor.create();
		
		this.paintA = NVGPaint.create();
		this.paintB = NVGPaint.create();
		this.paintC = NVGPaint.create();
	}
	
	public NVGColor rgba(float r, float g, float b, float a, NVGColor color) {
		color.r(r);
		color.g(g);
		color.b(b);
		color.a(a);
		
		return color;
	}
	
	public void renderString(ByteBuffer buffer, float x, float y, float size, String font, NVGColor color, int alignments) {
		nvgFontSize(vgHandle, size);
		nvgFontFace(vgHandle, font);
		nvgFillColor(vgHandle, color);
		nvgTextAlign(vgHandle, alignments);
		nvgText(vgHandle, x, y, buffer);
	}
	
	public void renderStringASCII(String string, float x, float y, float size, String font, NVGColor color, int alignments) {
		try (MemoryStack stack = stackPush()) {
			renderString(stack.ASCII(string, false), x, y, size, font, color, alignments);
        }
	}
	
	public void renderStringUTF8(String string, float x, float y, float size, String font, NVGColor color, int alignments) {
		try (MemoryStack stack = stackPush()) {
			renderString(stack.UTF8(string, false), x, y, size, font, color, alignments);
        }
	}
	
	public void setIconFont(String path) {
		try {
			iconFont = Util.loadResource(path, 450*1024);
			nvgCreateFontMem(vgHandle, "icons", iconFont, 0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setPlainFont(String path) {
		try {
			plainFont = Util.loadResource(path, 450*1024);
			nvgCreateFontMem(vgHandle, "plain", plainFont, 0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public long getWindowHandle() {
		return this.windowhandle;
	}
	
	public long getVG() {
		return this.vgHandle;
	}
	
	public void setFramebufferWidth(int framebufferWidth) {
		this.framebufferHeight = framebufferWidth;
	}
	
	public void setFramebufferHeight(int framebufferHeight) {
		this.framebufferHeight = framebufferHeight;
	}
	
	public void updateFramebufferSize(int width, int height) {
		this.framebufferWidth = width;
		this.framebufferHeight = height;
	}
	
	public int getFramebufferWidth() {
		return this.framebufferWidth;
	}
	
	public int getFramebufferHeight() {
		return this.framebufferHeight;
	}
	
	public float getPixelRatio() {
		return this.framebufferWidth/(float)this.framebufferHeight;
	}
	
	public void setViewport(int left, int top, int right, int bottom) {
		this.left = left;
		this.top = top;
		this.right = right;
		this.bottom = bottom;
		this.isMouseInView = Collision2D.pointBox2P(mouseX, mouseY, this.left, this.top, this.right, this.bottom);
	}
	
	public void subViewport(int left, int top, int right, int bottom) {
		if (left > this.left) {
			this.left = left;
		}
		if (top > this.top) {
			this.top = top;
		}
		if (right < this.right) {
			this.right = right;
		}
		if (bottom < this.bottom) {
			this.bottom = bottom;
		}
		this.isMouseInView = Collision2D.pointBox2P(mouseX, mouseY, this.left, this.top, this.right, this.bottom);
	}
	
	public int getLeft() {
		return this.left;
	}
	
	public int getTop() {
		return this.top;
	}
	
	public int getRight() {
		return this.right;
	}
	
	public int getBottom() {
		return this.bottom;
	}
	
	public void setColorScheme(ColorScheme colorScheme) {
		this.colorScheme = colorScheme;
	}
	
	public ColorScheme getColorScheme() {
		return this.colorScheme;
	}
	
	public float getCornerRadius() {
		return this.cornerRadius;
	}
	
	public void setCornerRadius(float cornerRadius) {
		this.cornerRadius = cornerRadius;
	}
	
	public int getHeaderHeight() {
		return this.headerHeight;
	}
	
	public void setHeaderheight(int headerHeight) {
		this.headerHeight = headerHeight;
	}
	
	public static void setMouseX(int x) {
		mouseX = x;
	}
	
	public static int getMouseX() {
		return mouseX;
	}
	
	public static void setMouseY(int y) {
		mouseY = y;
	}
	
	public static int getMouseY() {
		return mouseY;
	}
	
	public boolean isMouseInView() {
		return this.isMouseInView;
	}
	
	public boolean isPointInView(int px, int py) {
		return Collision2D.pointBox2P(px, py, left, top, right, bottom);
	}
	
	public boolean isMouseButtonPressed(int button) {
		return glfwGetMouseButton(windowhandle, button) == GLFW_PRESS;
	}
	
}
