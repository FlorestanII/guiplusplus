package de.johannespollitt.guiplusplus;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.nanovg.NanoVG.*;
import static org.lwjgl.system.MemoryUtil.*;

import java.nio.ByteBuffer;

import org.lwjgl.nanovg.NVGGlyphPosition;
import org.lwjgl.nanovg.NVGGlyphPosition.Buffer;
import org.lwjgl.nanovg.NVGPaint;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

public class NumberField extends InputField {
	
	private String text;
	private ByteBuffer textBuffer;
	
	private int cursorIndex = 0;
	
	private boolean focused = false;
	
	public NumberField() {
		this("");
	}
	
	public NumberField(String text) {
		this.text = text;
		this.textBuffer = memUTF8(text, false);
	}
	
	@Override
	public void render(View view) {
		//Background
		if (focused) {
			nvgBeginPath(view.getVG());
			nvgRoundedRect(view.getVG(), 0, 0, getWidth(), getHeight(), view.getCornerRadius());
			nvgFillColor(view.getVG(), view.rgba(0.19f, 0.21f, 0.22f, 1.0f, view.colorA));
			nvgFill(view.getVG());
		} else {
			nvgBeginPath(view.getVG());
			nvgRoundedRect(view.getVG(), 0, 0, getWidth(), getHeight(), view.getCornerRadius());
			nvgFillColor(view.getVG(), view.rgba(0.21f, 0.23f, 0.24f, 1.0f, view.colorA));
			nvgFill(view.getVG());
		}
		
		//Drop Shadow
		NVGPaint shadowPaint = view.paintA;
		nvgBoxGradient(view.getVG(), 0, 0 + 2, getWidth(), getHeight(), view.getCornerRadius() * 2, 10, view.getColorScheme().getShadowColor(), view.rgba(0.0f, 0.0f, 0.0f, 0.0f, view.colorB), shadowPaint);
		nvgBeginPath(view.getVG());
		nvgRoundedRect(view.getVG(), -10, -10, getWidth() + 20, getHeight() + 20, view.getCornerRadius());
		nvgRoundedRect(view.getVG(), 0, 0, getWidth(), getHeight(), view.getCornerRadius());
		nvgPathWinding(view.getVG(), NVG_HOLE);
		nvgFillPaint(view.getVG(), shadowPaint);
		nvgFill(view.getVG());
	}
	
	@Override
	public void renderChildren(View view) {
		
		//String width
		nvgSave(view.getVG());
		nvgFontFace(view.getVG(), "plain");
		nvgFontSize(view.getVG(), getHeight()*0.8f);
		nvgTextAlign(view.getVG(), NVG_ALIGN_LEFT);
		Buffer buffer = NVGGlyphPosition.malloc(cursorIndex+1);
		nvgTextGlyphPositions(view.getVG(), 0, 0, textBuffer, buffer);
		
		nvgTextAlign(view.getVG(), NVG_ALIGN_RIGHT);
		Buffer buffer2 = NVGGlyphPosition.malloc(1);
		nvgTextGlyphPositions(view.getVG(), 0, 0, textBuffer, buffer2);
		nvgRestore(view.getVG());
		
		float offset = 0;
		
		//Text
		if (-buffer2.get(0).x() > getWidth()) {
			offset = buffer2.get(0).x() + getWidth();
			view.renderString(textBuffer, buffer2.get(0).x() + getWidth(), getHeight()/2, getHeight()*0.8f, "plain", view.rgba(1, 1, 1, 1, view.colorA), NVG_ALIGN_LEFT | NVG_ALIGN_MIDDLE);
		} else {
			view.renderString(textBuffer, 0, getHeight()/2, getHeight()*0.8f, "plain", view.rgba(1, 1, 1, 1, view.colorA), NVG_ALIGN_LEFT | NVG_ALIGN_MIDDLE);
		}
		
		//Cursor
		if (cursorIndex == text.length()) {
			if (System.currentTimeMillis() % 1000 > 500 && focused) {
				view.renderString(view.icon_cursor, offset - buffer2.get(0).x(), getHeight()/2, getHeight()*0.8f, "plain", view.rgba(1, 1, 1, 1, view.colorA), NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
			}
			
		} else if (cursorIndex < text.length() && cursorIndex >= 0) {
			offset += buffer.get(cursorIndex).x();
			
			if (System.currentTimeMillis() % 1000 > 500 && focused) {
				view.renderString(view.icon_cursor, offset, getHeight()/2, getHeight()*0.8f, "plain", view.rgba(1, 1, 1, 1, view.colorA), NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
				buffer.free();
			}
						
		}
		
		buffer2.free();
		
	}
	
	@Override
	public boolean charInput(char c) {
		if (focused && (Character.isDigit(c) || c=='.')) {
			text = text.substring(0, cursorIndex) + c + text.substring(cursorIndex, text.length());
			cursorIndex++;
			memFree(this.textBuffer);
			this.textBuffer = memUTF8(text, false);
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean moveMouse(int mouseX, int mouseY, int lastMouseX, int lastMouseY, View view) {
		return false;
	}

	@Override
	public boolean clickMouse(int button, int action, View view) {
		this.focused = true;
		return false;
	}

	@Override
	public void clickOutside() {
		this.focused = false;
	}
	
	@Override
	public boolean pressKey(int key, boolean repeated) {
		if (focused) {
			if (key == GLFW_KEY_BACKSPACE && text.length() > 0) {
				
				if (cursorIndex == text.length()) {
					text = text.substring(0, text.length()-1);
					cursorIndex--;
				} else if (cursorIndex < text.length() && cursorIndex > 0){
					text = text.substring(0, cursorIndex-1) + text.substring(cursorIndex, text.length());
					cursorIndex--;
				}
				memFree(this.textBuffer);
				this.textBuffer = memUTF8(text, false);
				return true;
			} else if (key == GLFW_KEY_LEFT) {
				cursorIndex--;
				if (cursorIndex < 0) {
					cursorIndex = 0;
				}
				return true;
			} else if (key == GLFW_KEY_RIGHT) {
				cursorIndex++;
				if (cursorIndex > text.length()) {
					cursorIndex = text.length();
				}
				return true;
			}
		}
		
		return false;
	}

	@Override
	public boolean releaseKey(int key) {
		return false;
	}
	
	public void setNumber(float number) {
		this.text = Float.toString(number);
		memFree(this.textBuffer);
		this.textBuffer = memUTF8(this.text, false);
	}
	
	public float getNumber() {
		return Float.parseFloat(this.text);
	}
	
	public void setFocused(boolean focused) {
		this.focused = focused;
	}
	
	public boolean isFocused() {
		return this.focused;
	}
	
}
