package de.johannespollitt.guiplusplus;

import static org.lwjgl.nanovg.NanoVG.*;
import static org.lwjgl.system.MemoryUtil.*;

import java.nio.ByteBuffer;

import org.lwjgl.nanovg.NVGPaint;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

public class Button extends GuiComponent {
	
	private String label;
	private ByteBuffer labelBuffer;
	
	private boolean mouseOver = false;
	private boolean pressed = false;
	
	private Runnable action;
	
	public Button(String label) {
		this.label = label;
		this.labelBuffer = memUTF8(label, false);
	}
	
	@Override
	public void render(View view) {
		//Button
		NVGPaint headerPaint = view.paintB;
		
		if (mouseOver && !pressed) {
			nvgLinearGradient(view.getVG(), 0, 0, 0, getHeight()/2, view.rgba(0.26f, 0.27f, 0.28f, 1.0f, view.colorA), view.rgba(0.15f, 0.16f, 0.17f, 1.0f, view.colorB), headerPaint);
		} else if (mouseOver && pressed){
			nvgLinearGradient(view.getVG(), 0, 0, 0, getHeight()/2, view.rgba(0.23f, 0.24f, 0.25f, 1.0f, view.colorA), view.rgba(0.12f, 0.13f, 0.14f, 1.0f, view.colorB), headerPaint);
		} else {
			nvgLinearGradient(view.getVG(), 0, 0, 0, getHeight()/2, view.rgba(0.25f, 0.26f, 0.27f, 1.0f, view.colorA), view.rgba(0.14f, 0.15f, 0.16f, 1.0f, view.colorB), headerPaint);
		}
		
		nvgBeginPath(view.getVG());
		nvgRoundedRect(view.getVG(), 0, 0, getWidth(), getHeight(), view.getCornerRadius());
		nvgFillPaint(view.getVG(), headerPaint);
		nvgFill(view.getVG());
		
		//Drop Shadow
		NVGPaint shadowPaint = view.paintA;
		nvgBoxGradient(view.getVG(), 0, 0 + 2, getWidth(), getHeight(), view.getCornerRadius() * 2, 10, view.getColorScheme().getShadowColor(), view.rgba(0.0f, 0.0f, 0.0f, 0.0f, view.colorB), shadowPaint);
		nvgBeginPath(view.getVG());
		nvgRoundedRect(view.getVG(), -10, -10, getWidth() + 20, getHeight() + 20, view.getCornerRadius());
		nvgRoundedRect(view.getVG(), 0, 0, getWidth(), getHeight(), view.getCornerRadius());
		nvgPathWinding(view.getVG(), NVG_HOLE);
		nvgFillPaint(view.getVG(), shadowPaint);
		nvgFill(view.getVG());
		
		//Label
		view.renderString(labelBuffer, getWidth()/2, getHeight()/2, view.getHeaderHeight(), "plain", view.getColorScheme().getFrameTitleColor(), NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
		
	}

	@Override
	public boolean moveMouse(int mouseX, int mouseY, int lastMouseX, int lastMouseY, View view) {
		mouseOver = true;
		return false;
	}
	
	@Override
	public void moveMouseOutside() {
		mouseOver = false;
		pressed = false;
	}
	
	@Override
	public boolean clickMouse(int button, int action, View view) {
		if (button == 0 && action == 1 && mouseOver) {
			pressed = true;
		} else if (button == 0 && action == 0 && mouseOver && pressed) {
			pressed = false;
			if (this.action != null) {
				this.action.run();
			}
		} else {
			pressed = false;
		}
		return false;
	}

	@Override
	public boolean pressKey(int key, boolean repeated) {
		return false;
	}

	@Override
	public boolean releaseKey(int key) {
		return false;
	}
	
	public String getLabel() {
		return this.label;
	}
	
	public void setLabel(String label) {
		this.label = label;
		memFree(labelBuffer);
		labelBuffer = memUTF8(label, false);
	}
	
	public void setAction(Runnable action) {
		this.action = action;
	}
	
}
