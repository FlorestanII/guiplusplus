package de.johannespollitt.guiplusplus;

import static org.lwjgl.nanovg.NanoVG.*;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

public class Gui {
	
	private GuiContainer container;
	
	private int lastMouseX;
	private int lastMouseY;
	
	public Gui() {
		this.container = new GuiContainer() {
			
			@Override
			public void render(View view) {
			}
			
			@Override
			public boolean moveMouse(int mouseX, int mouseY, int lastMouseX, int lastMouseY, View view) {
				return false;
			}
			
			@Override
			public boolean clickMouse(int button, int action, View view) {
				return false;
			}
			
			@Override
			public boolean pressKey(int key, boolean repeated) {
				return false;
			}
			
			@Override
			public boolean releaseKey(int key) {
				return false;
			}
		};
	}
	
	public void render(View view) {
		nvgBeginFrame(view.getVG(), view.getFramebufferWidth(), view.getFramebufferHeight(), view.getPixelRatio());
		if (container.isVisible()) {
			container.setBounds(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
			
			if (View.getMouseX() != lastMouseX || View.getMouseY() != lastMouseY) {
				container.moveMouseChildren(View.getMouseX(), View.getMouseY(), lastMouseX, lastMouseY, view);
			}
			
			container.renderChildren(view);
		}
		nvgEndFrame(view.getVG());
		
		lastMouseX = View.getMouseX();
		lastMouseY = View.getMouseY();
	}
	
	//Events
	public boolean clickMouse(int button, int action, View view) {
		return container.clickMouseChildren(button, action, view);
	}
	
	public boolean pressKey(int key, boolean repeated) {
		return container.pressKeyChildren(key, repeated);
	}
	
	public boolean releaseKey(int key) {
		return container.releaseKeyChildren(key);
	}
	
	public boolean charInput(char c) {
		return container.charInputChildren(c);
	}
	
	public void add(GuiComponent component) {
		container.add(component);
	}
	
	public void setVisible(boolean visible) {
		this.container.setVisible(visible);
	}
	
	public boolean isVisible() {
		return this.container.isVisible();
	}
	
}
